from django.db import models

# Create your models here.
class Sensor(models.Model):
  SENSOR_TYPES = (
      ('Temperature', 'Easy'),
      ('Humidity', 'Humidity'),
      ('Lighting', 'Lighting'),
  )
  name = models.CharField(max_length=120)
  description = models.CharField(max_length=400)
  picture = models.FileField(blank=True)
  type = models.CharField(choices=SENSOR_TYPES, max_length=10)
  value = models.PositiveIntegerField()
  time_stamp = models.DateTimeField("", auto_now=False, auto_now_add=False)
  others = models.TextField()

  def __str_(self):
      return "Sensor for {}".format(self.name)