from django.shortcuts import render

from rest_framework import viewsets
from .serializers import SensorSerializer
from .models import Sensor

# Create your views here.

class SensorViewSet(viewsets.ModelViewSet):
  serializer_class = SensorSerializer
  queryset = Sensor.objects.all()
