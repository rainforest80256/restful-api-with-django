from rest_framework import serializers
from .models import Sensor

class SensorSerializer(serializers.ModelSerializer):
  class Meta:
    model = Sensor
    fields = ("id", "name", "description", "picture", "type", "value", "time_stamp", "others")