## Django

- Install Django

  ```powershell
  pip install Django
  ```

- Create Project

  ```powershell
  django-admin startproject <Project Name>
  ```

  - add [CORS](https://developer.mozilla.org/zh-TW/docs/Web/HTTP/CORS) Support

    ```powershell
    pip install django-cors-headers django-rest-framework
    ```

    - edit **<project>/settings.py**

      ```python
      INSTALLED_APPS = [
      	...,
      	# add django app that we just created and its dependancies
      	'rest_framework',
      	'corsheaders',
      ]

      MIDDLWWARE = [
      	...,
      	'corsheaders.middleware.CorsMiddleware'
      ]

      # add your domain to whitelist
      CORS_ORIGIN_WHITELIST = (
          'localhost:3000',
      )
      ```

- Create Model for Database

  - Create Django app

    ```powershell
    python manage.py startapp <App Name>
    ```

    - Registering app you just created in **<project>/settings.py**

      ```python
      INSTALLED_APPS = [
      	...,
      	'<App Name>'
      ]
      ```

  - Edit **models.py** under **<app>/**

    ```python
    from django.db import models

    class Sensor(models.Model):
      SENSOR_TYPES = (
          ('Temperature', 'Easy'),
          ('Humidity', 'Humidity'),
          ('Lighting', 'Lighting'),
      )
      name = models.CharField(max_length=120)
      description = models.CharField(max_length=400)
      picture = models.FileField(blank=True)
      type = models.CharField(choices=SENSOR_TYPES, max_length=10)
      value = models.PositiveIntegerField()
      time_stamp = models.DateTimeField("", auto_now=False, auto_now_add=False)
      others = models.TextField()

      def __str_(self):
          return "Sensor for {}".format(self.name)
    ```

    - [Model field reference](https://docs.djangoproject.com/en/2.2/ref/models/fields/)

    - Register your model in **<app>/admin.py**

      ```python
      from django.contrib import admin

      from .models import Sensor

      admin.site.register(Sensor)
      ```

* Add **serializers.py** under **<app>/** to serialize your models

  ```python
  from rest_framework import serializers
  from .models import Sensor

  class SensorSerializer(serializers.ModelSerializer):
    class Meta:
      model = Sensor
      fields = ("id", "name", "description", "picture", "type", "value", "time_stamp", "others")
  ```

* Create view for model in **<app>/views.py**

  ```python
  from django.shortcuts import render

  from rest_framework import viewsets
  from .serializers import SensorSerializer
  from .models import Sensor

  class SensorViewSet(viewsets.ModelViewSet):
    serializer_class = SensorSerializer
    queryset = Sensor.objects.all()
  ```

* Dealing with Router

  - in **<app>/urls.py**,

    ```python
    from django.urls import path, include
    from rest_framework.routers import DefaultRouter
    from .views import SensorViewSet

    router = DefaultRouter()
    # http://127.0.0.1:8000/api/sensors/
    router.register(r'sensors', SensorViewSet)

    urlpatterns = [
        path("", include(router.urls))
    ]
    ```

  - in **<project>/urls.py**,

    ```python
    from django.contrib import admin
    # add following lines
    from django.urls import path, include
    from django.conf import settings
    from django.conf.urls.static import static

    urlpatterns = [
        path('admin/', admin.site.urls),
        path('api/', include('core.urls'))
    ]

    if settings.DEBUG:
        urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    ```

    - Create static folder to store images that you have uploaded. In **<project>/settings.py**,

      ```python
      ...
      STATIC_URL = '/static/'

      # add following lines
      MEDIA_URL = '/media/'
      MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
      ```
    
  - Apply your changes of Django app to the project

    ```powershell
    python manage.py makemigrations
    python manage.py migrate
    ```

* Run server

  ```powershell
  python manage.py runserver
  ```

  

* References

  1. [Home - Django REST framework](https://www.django-rest-framework.org/)
     - A simple GUI for restful api functions
